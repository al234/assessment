package aprj;


import static org.junit.Assert.*;

import java.io.FileNotFoundException;

import org.junit.Test;
import org.junit.rules.ExpectedException;

public class TestFxquote
{
    public final ExpectedException exception = ExpectedException.none();
      
    @Test
    //(expected = FileNotFoundExcetion.class)
    public void  testForInvalidFileName()throws Exception{
        Fxquote f = new Fxquote("invalid");
        exception.expect(FileNotFoundException.class);
        
    }
    
    @Test
    public void testZMKZAR(){
       Fxquote f = new Fxquote("rates.csv");
      try{ assertEquals(0.0014,f.getFXQuote("ZMK","ZAR"),0.0001);}
      catch(Exception e){}
    }
    
    @Test
    public void testIndirectQuotation(){
        Fxquote f = new Fxquote("rates.csv");
        try{assertEquals(0.1505*1763.0,f.getFXQuote("XPT","ZAR"),0.0001);}
        catch(Exception e){}
     }
    
    @Test
    public void testNotExistRate(){
        Fxquote f = new Fxquote("rates.csv");
       try{ assertEquals(0.1505*1763.0,f.getFXQuote("AAD","ZAR"),0.0001);}
        catch(Exception e){}
     }
    @Test
    public void testCADUSD(){
       Fxquote f = new Fxquote("rates.csv");
      try{ assertEquals(1.0069,f.getFXQuote("CAD","USD"),0.0001);}
      catch(Exception e){}
    }
    
    
    @Test
    public void testCADCAD(){
       Fxquote f = new Fxquote("rates.csv");
      try{ assertEquals(1.0,f.getFXQuote("CAD","CAD"),0.0001);}
      catch(Exception e){}
    }
    
     
        
}
